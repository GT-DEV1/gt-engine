
local MainScene = class("MainScene", cc.load("mvc").ViewBase)

function MainScene:onCreate()
    cc.debug_mode_ = true
    self:initiate()

    --cc.lite_edition_ = true

    display.setAutoScale({autoscale = "SHOW_ALL", width = 256, height = 224}, {width = display.width, height = display.height})

    -- add background image
    display.newSprite("MainSceneBg.jpg")
        :move(display.center)
        :addTo(self)

    -- add play button
    local playButton = cc.MenuItemImage:create("PlayButton.png", "PlayButton.png")
        :onClicked(function()
            self:getApp():enterScene("title.opening", "FADE", 1)
        end)
    cc.Menu:create(playButton)
        :move(display.center)
        :addTo(self)
end

function MainScene:initiate()

    cc.levels = {}

    local level_ids = {}
    local level_weapons = {}

    level_ids[#level_ids + 1] = "level_freezerman"
    level_ids[#level_ids + 1] = "level_sheriffman"
    level_ids[#level_ids + 1] = "level_boomerman"
    level_ids[#level_ids + 1] = "level_militaryman"
    level_ids[#level_ids + 1] = "level_vineman"
    level_ids[#level_ids + 1] = "level_shieldman"
    level_ids[#level_ids + 1] = "level_nightman"
    level_ids[#level_ids + 1] = "level_torchman"
    level_ids[#level_ids + 1] = "level_test"

    level_weapons[#level_weapons + 1] = "freezer"
    level_weapons[#level_weapons + 1] = "sheriff"
    level_weapons[#level_weapons + 1] = "boomer"
    level_weapons[#level_weapons + 1] = "military"
    level_weapons[#level_weapons + 1] = "vine"
    level_weapons[#level_weapons + 1] = "shield"
    level_weapons[#level_weapons + 1] = "night"
    level_weapons[#level_weapons + 1] = "torch"
    level_weapons[#level_weapons + 1] = "noone"

    for i = 1, #level_ids do
        local level_map = {}
        level_map["id"] = level_ids[i]
        level_map["defeated"] = false
        level_map["weapon"] = level_weapons[i]
        cc.levels[#cc.levels + 1] = level_map
    end

    cc.current_level = nil;

end

return MainScene