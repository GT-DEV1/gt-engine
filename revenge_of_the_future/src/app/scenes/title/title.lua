
-- GameView is a combination of view and controller
local title         = class("title", cc.load("mvc").ViewBase)
local background    = import("app.objects.background")
local text_element  = import("app.objects.ui.text_element")
local switch        = import("app.system.switch")

function title:onCreate()

    -- controller array, node container, previous controller, current controller and controller index
    self.scene_components_ = {}

    -- add keypad layer
    self.keypad_layer_ = display.newLayer()
                                :onKeypad(handler(self, self.onKeypad))
                                :addTo(self)

    self.background_ = background:create("backgrounds/screens/title/bg_titlescreen.png", 1, 0.2, display.center, 0)
                                 :start({is_loop = false, remove_self = false})
                                 :addTo(self, 0)

    self.cursor_ = background:create("sprites/common/spr_small_cursor_right.png", 1, 0.0, display.center, 0)
                             :start({is_loop = false, remove_self = false})
                             :addTo(self, 0)

    self.text_ = text_element:create("start game", "fonts/megaman_2.ttf", 8)
                             :addTo(self, 100)

    self.text_:set_debug_draw_node(self)
    self.cursor_:set_debug_draw_node(self)



    self.text_:getTexture()
              :setAliasTexParameters()


    self.text_:setAnchorPoint(0, 1)
    self.cursor_:setAnchorPoint(0, 1)

    self.cursor_:setPosition(cc.p(64, 96))

    self.text_:setPosition(cc.p(self.cursor_:getBoundingBox().x + self.cursor_:getBoundingBox().width,
                                self.cursor_:getBoundingBox().y +
                                        self.cursor_:getBoundingBox().height * 0.5 +
                                        self.text_:getBoundingBox().height * 0.5))


    self.scene_components_[#self.scene_components_ + 1] = self.text_
    self.scene_components_[#self.scene_components_ + 1] = self.cursor_

--  audio.playMusic("sounds/bgm_title.mp3", true)

    -- binding to the "event" component
        cc.bind(self, "event")


    -- self variables
    self.triggered_ = false

    -- schedule update
    self:start()
end


function title:onKeypad(event)

--    print(cc.KeyCodeKey[keyCode + 1])

--    if is_pressed then
--        print("true")
--    else
--        print("false")
--    end

    --print(tostring(event["key"]) .. " and " .. tostring(event["pressed"]))


    switch {
        ["KEY_KP_ENTER"] = function()
                                if not self.triggered_ then
                                    self.triggered_ = true
                                    audio.playSound("sounds/sfx_selected.wav")
                                    self:getApp():enterScene("gameplay.stage_select", "FADE", 1)
                                end
                            end,
--        ["KEY_UP_ARROW"] = function () audio.playSound("sounds/sfx_select.wav") end,
--        ["KEY_DOWN_ARROW"] = function () audio.playSound("sounds/sfx_select.wav") end,
        default = function ()  end,
    }:case(cc.KeyCodeKey[event["key"] + 1])

end


function title:start()
    self:scheduleUpdate(handler(self, self.step))
    return self
end

function title:stop()
    self:unscheduleUpdate()
    return self
end

function title:step(dt)
    for _, scene_component in pairs(self.scene_components_) do
        scene_component:step(dt)
    end

    return self
end

function title:onCleanup()
    self.keypad_layer_:removeKeypad():removeSelf()
    self.background_:destroy(true)
    self.text_:removeSelf()
    self:removeAllEventListeners()
end

return title