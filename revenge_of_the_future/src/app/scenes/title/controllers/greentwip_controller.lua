--
-- Created by Victor on 6/23/2015 1:13 PM
--
local base_controller = import(".base_controller")

local greentwip_controller = class("greentwip_controller", base_controller)

function greentwip_controller:start()
    self.background_frames_ = 4

    self:add_texture("backgrounds/screens/opening/greentwip_logo/bg_greentwip_a.png")
    self:add_texture("backgrounds/screens/opening/greentwip_logo/bg_greentwip_b.png")
    self:add_texture("backgrounds/screens/opening/greentwip_logo/bg_greentwip_c.png")
    self:add_texture("backgrounds/screens/opening/greentwip_logo/bg_greentwip_d.png")

    self:load_background(self.background_index_)

    return self
end

return greentwip_controller