--
-- Created by Victor on 6/23/2015 1:13 PM
--

local base_controller = import(".base_controller")

local flematico_controller = class("flematico_controller", base_controller)


function flematico_controller:start()
    self.background_frames_ = 1
    self:add_texture("backgrounds/screens/opening/flematico_logo/bg_flematico.png")
    self:load_background(self.background_index_)
    return self
end

return flematico_controller