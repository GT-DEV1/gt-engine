--
-- Created by Victor on 6/23/2015 1:13 PM
--

local background = import("app.objects.background")

local base_controller = class("base_controller", function()
    return cc.Node:create()
end)


function base_controller:ctor()
    self.done_ = false
    self.background_frames_ = 1
    self.textures_ = {}
    self.backgrounds_node_ = display.newNode():addTo(self)
    self.current_background_ = nil
    self.previous_background_ = nil
    self.background_index_ = 1
end

function base_controller:destroy()
    if self.current_background_ ~= nil then
        self.current_background_:destroy(true)
        self.current_background_ = nil
    end

    self:removeSelf()
end

function base_controller:add_texture(texture_name)
    self.textures_[#self.textures_ + 1] = texture_name
end

function base_controller:load_background(background_index)

    local new_background =  background:create(self.textures_[background_index], self.background_frames_, 0.2, display.center, 0):
                                       start({is_loop = false, remove_self = false}):
                                       addTo(self.backgrounds_node_, 100)
    self.current_background_ = new_background
end

function base_controller:step(dt)

    if self.background_index_ < #self.textures_ then
        if self.current_background_:animation_ended() then

            if self.previous_background_ ~= nil then
                self.previous_background_:destroy(true)
                self.previous_background_ = nil
            end

            self.previous_background_ = self.current_background_

            self.background_index_ = self.background_index_ + 1
            self:load_background(self.background_index_)

        end
    else
        if self.current_background_:animation_ended() then
            self.done_ = true

            if self.previous_background_ ~= nil then
                self.previous_background_:destroy(true)
                self.previous_background_ = nil
            end

        end
    end

end

function base_controller:done()
    return self.done_
end

return base_controller

