-- GameView is a combination of view and controller
local opening = class("opening", cc.load("mvc").ViewBase)

local greentwip_controller = import(".controllers.greentwip_controller")
local flematico_controller = import(".controllers.flematico_controller")

--GameView.HOLE_POSITION = cc.p(display.cx - 30, display.cy - 75)
--GameView.INIT_LIVES = 5
--GameView.ADD_BUG_INTERVAL_MIN = 1
--GameView.ADD_BUG_INTERVAL_MAX = 3

--GameView.IMAGE_FILENAMES = {}
--GameView.IMAGE_FILENAMES[BugBase.BUG_TYPE_ANT] = "BugAnt.png"
--GameView.IMAGE_FILENAMES[BugBase.BUG_TYPE_SPIDER] = "BugSpider.png"

--GameView.BUG_ANIMATION_TIMES = {}
--GameView.BUG_ANIMATION_TIMES[BugBase.BUG_TYPE_ANT] = 0.15
--GameView.BUG_ANIMATION_TIMES[BugBase.BUG_TYPE_SPIDER] = 0.1

--GameView.ZORDER_BUG = 100
--GameView.ZORDER_DEAD_BUG = 50

function opening:onCreate()
    -- controller array, node container, previous controller, current controller and controller index
    self.controllers_ = {}
    self.controllers_node_ = display.newNode():addTo(self)
    self.previous_controller_ = nil
    self.current_controller_ = nil
    self.controller_index_ = 1

    -- the opening controllers
    self.controllers_[1] = greentwip_controller
    self.controllers_[2] = flematico_controller

    -- binding to the "event" component
    cc.bind(self, "event")


    -- add touch layer
    --    display.newLayer()
    --        :onTouch(handler(self, self.onTouch))
    --        :addTo(self)

    -- add background image
    --    display.newSprite("PlaySceneBg.jpg")
    --        :move(display.center)
    --        :addTo(self)


    --    local bug = BugSprite:create(GameView.IMAGE_FILENAMES[bugType], bugModel)
    --    :start(GameView.HOLE_POSITION)
    --    :addTo(self.bugsNode_, GameView.ZORDER_BUG)
    -- add lives icon and label
    --    display.newSprite("Star.png")
    --        :move(display.left + 50, display.top - 50)
    --        :addTo(self)
    --    self.livesLabel_ = cc.Label:createWithSystemFont(self.lives_, "Arial", 32)
    --        :move(display.left + 90, display.top - 50)
    --        :addTo(self)


    self:start()

end

function opening:onTouch(event)
    --    if event.name ~= "began" then return end
    --    local x, y = event.x, event.y
    --    for _, bug in pairs(self.bugs_) do
    --        if bug:getModel():checkTouch(x, y) then
    --            self:bugDead(bug)
    --        end
    --    end
end

function opening:start()
    self:scheduleUpdate(handler(self, self.step))
    return self
end

function opening:stop()
    self:unscheduleUpdate()
    return self
end

function opening:step(dt)

    --    if self.lives_ <= 0 then return end

    --    self.addBugInterval_ = self.addBugInterval_ - dt
    --    if self.addBugInterval_ <= 0 then
    --        self.addBugInterval_ = math.random(GameView.ADD_BUG_INTERVAL_MIN, GameView.ADD_BUG_INTERVAL_MAX)
    --        self:addBug()
    --    end

    if self.current_controller_ == nil then
        self.current_controller_ = self.controllers_[self.controller_index_]:create():start()
        self.current_controller_:addTo(self.controllers_node_, 150)
    end

    self.current_controller_:step(dt)

    if self.current_controller_:done() then
        if self.controller_index_ < #self.controllers_ then

            if self.previous_controller_ ~= nil then
                self.previous_controller_:destroy()
                self.previous_controller_ = nil
            end

            self.previous_controller_ = self.current_controller_
            self.controller_index_ = self.controller_index_ + 1

            self.current_controller_ = self.controllers_[self.controller_index_]:create():start()
            self.current_controller_:addTo(self.controllers_node_, 150)
        else

            if self.previous_controller_ ~= nil then
                self.previous_controller_:destroy()
                self.previous_controller_ = nil
            end
--            - @param self
            -- @param #float duration
            -- @param #cc.Scene scene
            -- @param #color3b_table color

--            cc.Director:getInstance():replaceScene(cc.TransitionFade:create(0.5, title, cc.c3b(0,255,255)));
--            display.runScene(scene, "FADE", 1)
            self:stop()
            self:getApp():enterScene("title.title", "FADE", 1)
--            function()
--                self:getApp():enterScene("title")
--            end
--            self:dispatchEvent({name = GameView.events.PLAYER_DEAD_EVENT,
--                next_scene = "var"})

        end

    end

    return self
end

function opening:onCleanup()
    if self.previous_controller_ ~= nil then
        self.previous_controller_:destroy()
        self.previous_controller_ = nil
    end

    if self.current_controller_ ~= nil then
        self.current_controller_:destroy()
        self.current_controller_ = nil
    end

    for i = 1, #self.controllers_ do
        self.controllers_[i] = nil
    end

    self:removeAllEventListeners()
end

function opening:addBug()

--[[    local bugType = BugBase.BUG_TYPE_ANT
    if math.random(1, 2) % 2 == 0 then
        bugType = BugBase.BUG_TYPE_SPIDER
    end

    local bugModel
    if bugType == BugBase.BUG_TYPE_ANT then
        bugModel = BugAnt:create()
    else
        bugModel = BugSpider:create()
    end

    local bug = BugSprite:create(GameView.IMAGE_FILENAMES[bugType], bugModel)
        :start(GameView.HOLE_POSITION)
        :addTo(self.bugsNode_, GameView.ZORDER_BUG)

    self.bugs_[bug] = bug ]]--
    return self
end

function opening:bugEnterHole(bug)
--[[    self.bugs_[bug] = nil

    bug:fadeOut({time = 0.5, removeSelf = true})
        :scaleTo({time = 0.5, scale = 0.3})
        :rotateTo({time = 0.5, rotation = math.random(360, 720)})

    self.lives_ = self.lives_ - 1
    self.livesLabel_:setString(self.lives_)
    audio.playSound("BugEnterHole.wav")

    if self.lives_ <= 0 then
        self:dispatchEvent({name = GameView.events.PLAYER_DEAD_EVENT,
                            next_scene = "var"})
    end
]]--
    return self
end

function opening:bugDead(bug)
--[[    local imageFilename = GameView.IMAGE_FILENAMES[bug:getModel():getType()]
    DeadBugSprite:create(imageFilename)
        :fadeOut({time = 2.0, delay = 0.5, removeSelf = true})
        :move(bug:getPosition())
        :rotate(bug:getRotation() + 120)
        :addTo(self.bugsNode_, GameView.ZORDER_DEAD_BUG)
]]--
--    self.bugs_[bug] = nil
--    bug:removeSelf()

--    self.kills_ = self.kills_ + 1
--    audio.playSound("BugDead.wav")

    return self
end

return opening