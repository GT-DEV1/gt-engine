--
-- Created by Victor on 6/27/2015 2:59 PM
--

local level_militaryman  = class("stage_select", cc.load("mvc").ViewBase)
local background    = import("app.objects.background")
local text_element  = import("app.objects.ui.text_element")
local switch        = import("app.system.switch")
local sprite        = import("app.objects.sprite")

function level_militaryman:onCreate()

    self.map_ = cc.TMXTiledMap:create("tilemaps/militaryman/level_militaryman.tmx")
                               :setAnchorPoint(cc.p(0,1))
                               :setPosition(display.left_top)
                               :addTo(self, 10)

    local children = self.map_:getChildren()
    for _, child in ipairs(children) do
        child:getTexture():setAliasTexParameters()
    end

    --[[
        -- controller array, node container, previous controller, current controller and controller index
        self.scene_components_ = {}

        -- add keypad layer
        self.keypad_layer_ = display.newLayer()
        :onKeypad(handler(self, self.onKeypad))
        :addTo(self)

        self.background_ = background:create("backgrounds/screens/stage_select/bg_stageselect_lite.png", 1, 0.2, display.center, 0)
        :start({is_loop = false, remove_self = false})
        :addTo(self, 0)

        self.cursor_ = background:create("sprites/screens/stage_select/spr_stage_select_cursor.png", 2, 0.4, display.center, 0)
        :start({is_loop = true, remove_self = false})
        :setAnchorPoint(0, 1)
        :addTo(self, 10)

        self.cursor_:set_position(cc.p(104, 144))

        self.text_ = text_element:create("stage select", "fonts/megaman_2.ttf", 8)
        :setAnchorPoint(0.5, 0.5)
        :addTo(self, 100)

        self.cursor_:set_debug_draw_node(self)
        self.text_:set_debug_draw_node(self)

        self.text_:setPosition(cc.p(display.center.x, display.top - self.text_:getBoundingBox().height * 0.5))

        local sprite_path = "sprites/screens/stage_select"
        self.cody_sprite_   = sprite:create(sprite_path .. "/spr_stage_select_cody.plist",
            sprite_path .. "/spr_stage_select_cody.png")
        :setAnchorPoint(0, 1)
        :setPosition(cc.p(104, 144))
        :addTo(self)


        self.cursor_.x_position = "middle"
        self.cursor_.y_position = "middle"

        --  audio.playMusic("sounds/bgm_title.mp3", true)


        -- self variables
        self.triggered_ = false

        self.scene_components_[#self.scene_components_ + 1] = self.text_
        self.scene_components_[#self.scene_components_ + 1] = self.cursor_

        self:setup_mugs()
        ]]--

    -- binding to the "event" component
    cc.bind(self, "event")

    -- schedule update
    self:start()
end

--function level_militaryman:getEyeX()
--    return this.sprite.getPositionX() - g_runnerStartX;
--end

function level_militaryman:onKeypad(event)

    if event["pressed"]  then
        switch {
            ["KEY_KP_ENTER"] = function()

            end,
            ["KEY_LEFT_ARROW"] = function ()

            end,
            ["KEY_RIGHT_ARROW"] = function ()

            end,
            ["KEY_UP_ARROW"] = function ()

            end,
            ["KEY_DOWN_ARROW"] = function ()

            end,
            default = function ()  end,
        }:case(cc.KeyCodeKey[event["key"] + 1])
    end

end

function level_militaryman:start()
    self:scheduleUpdate(handler(self, self.step))
    return self
end

function level_militaryman:stop()
    self:unscheduleUpdate()
    return self
end

function level_militaryman:step(dt)
    --    for _, scene_component in pairs(self.scene_components_) do
    --        scene_component:step(dt)
    --    end
    return self
end

function level_militaryman:onCleanup()
    --    self.keypad_layer_:removeKeypad():removeSelf()
    --    self.background_:destroy(true)
    --    self.text_:removeSelf()
    self:removeAllEventListeners()
end

return level_militaryman