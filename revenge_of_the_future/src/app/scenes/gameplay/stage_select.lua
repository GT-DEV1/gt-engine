--
-- Created by Victor on 6/22/2015 5:07 PM
--

local stage_select  = class("stage_select", cc.load("mvc").ViewBase)
local background    = import("app.objects.background")
local text_element  = import("app.objects.ui.text_element")
local switch        = import("app.system.switch")
local sprite        = import("app.objects.sprite")

function stage_select:onCreate()

    -- controller array, node container, previous controller, current controller and controller index
    self.scene_components_ = {}

    -- add keypad layer
    self.keypad_layer_ = display.newLayer()
    :onKeypad(handler(self, self.onKeypad))
    :addTo(self)

    self.background_ = background:create("backgrounds/screens/stage_select/bg_stageselect_lite.png", 1, 0.2, display.center, 0)
    :start({is_loop = false, remove_self = false})
    :addTo(self, 0)

    self.cursor_ = background:create("sprites/screens/stage_select/spr_stage_select_cursor.png", 2, 0.4, display.center, 0)
    :start({is_loop = true, remove_self = false})
    :setAnchorPoint(0, 1)
    :addTo(self, 10)

    self.cursor_:set_position(cc.p(104, 144))

    self.text_ = text_element:create("stage select", "fonts/megaman_2.ttf", 8)
                             :setAnchorPoint(0.5, 0.5)
                             :addTo(self, 100)

    self.cursor_:set_debug_draw_node(self)
    self.text_:set_debug_draw_node(self)

    self.text_:setPosition(cc.p(display.center.x, display.top - self.text_:getBoundingBox().height * 0.5))

    local sprite_path = "sprites/screens/stage_select"
    self.cody_sprite_   = sprite:create(sprite_path .. "/spr_stage_select_cody.plist",
                                        sprite_path .. "/spr_stage_select_cody.png")
                                :setAnchorPoint(0, 1)
                                :setPosition(cc.p(104, 144))
                                :addTo(self)


    self.cursor_.x_position = "middle"
    self.cursor_.y_position = "middle"

    --  audio.playMusic("sounds/bgm_title.mp3", true)

    -- binding to the "event" component
    cc.bind(self, "event")


    -- self variables
    self.triggered_ = false

    self.scene_components_[#self.scene_components_ + 1] = self.text_
    self.scene_components_[#self.scene_components_ + 1] = self.cursor_

    self:setup_mugs()

    -- schedule update
    self:start()
end

function stage_select:setup_mugs()
    self.mugs_ = {}

    local x_offset = 24
    local y_offset = 208

    for i = 1, #cc.levels do
        local key = cc.levels[i]
        if i == 5 then
            x_offset = x_offset + 80;
        end

        local foe_name = string.sub(key["id"], 7, -1)
        local drawable = false

        if foe_name == "sheriffman" or foe_name == "vineman" or foe_name == "militaryman" or foe_name == "nightman" then
            drawable = true
        end

        if not key["defeated"] then
            if drawable then
                local sprite_path = "sprites/screens/stage_select"

                self.mugs_[#self.mugs_ + 1] =   sprite:create(sprite_path .. "/spr_stage_select_mugs.plist",
                                                              sprite_path .. "/spr_stage_select_mugs.png")
                                                      :setAnchorPoint(0, 1)
                                                      :addTo(self)

                self.mugs_[#self.mugs_]:set_position(cc.p(x_offset, y_offset))
                self.mugs_[#self.mugs_]:set_image_index(i)
            end
        end


        if x_offset >= 184 then
            x_offset = 24;
            y_offset = y_offset - 64;
        else
            x_offset = x_offset + 80;
        end
    end
end

function stage_select:set_cody_sprite()

    local cody_sprite = 1

    if self.cursor_.x_position == 'middle' and self.cursor_.y_position == "middle" then

        cody_sprite = 1

    elseif self.cursor_.x_position == 'left' and self.cursor_.y_position == 'top' then

        cody_sprite = 2

    elseif self.cursor_.x_position == 'middle' and self.cursor_.y_position == 'top' then

        cody_sprite = 3

    elseif self.cursor_.x_position == 'right' and self.cursor_.y_position == 'top' then

        cody_sprite = 4

    elseif self.cursor_.x_position == 'right' and self.cursor_.y_position == 'middle' then

        cody_sprite = 5

    elseif self.cursor_.x_position == 'right' and self.cursor_.y_position == 'bottom' then

        cody_sprite = 6

    elseif self.cursor_.x_position == 'middle' and self.cursor_.y_position == 'bottom' then

        cody_sprite = 7

    elseif self.cursor_.x_position == "left" and self.cursor_.y_position == "bottom" then

        cody_sprite = 8

    else

        cody_sprite = 9

    end

    self.cody_sprite_:set_image_index(cody_sprite)

end

function stage_select:move_left()
    local play_fx = true;
    local move = true;

    if self.cursor_.y_position == "top" or self.cursor_.y_position == "bottom" then
        play_fx = false
        move = false
    end

    if move then
        if self.cursor_.x_position == "middle" then
            self.cursor_.x_position = 'left';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x-80, self.cursor_:get_position().y));
        elseif self.cursor_.x_position == "left" then
            self.cursor_.x_position = 'right';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x+160, self.cursor_:get_position().y));
        elseif self.cursor_.x_position == "right" then
            self.cursor_.x_position = 'middle';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x-80, self.cursor_:get_position().y));
        end
    end

    if play_fx then
        audio.playSound("sounds/sfx_select.wav")
    end
end

function stage_select:move_right()
    local play_fx = true;
    local move = true;

    if self.cursor_.y_position == "top" or self.cursor_.y_position == "bottom" then
        play_fx = false
        move = false
    end

    if move then
        if self.cursor_.x_position == "middle" then
            self.cursor_.x_position = 'right';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x+80, self.cursor_:get_position().y));
        elseif self.cursor_.x_position == "left" then
            self.cursor_.x_position = 'middle';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x+80, self.cursor_:get_position().y));
        elseif self.cursor_.x_position == "right" then
            self.cursor_.x_position = 'left';
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x-160, self.cursor_:get_position().y));
        end
    end

    if play_fx then
        audio.playSound("sounds/sfx_select.wav")
    end

end

function stage_select:move_up()

    local play_fx = true;
    local move = true;

    if self.cursor_.x_position == "left" or self.cursor_.x_position == "right" then
        play_fx = false
        move = false
    end

    if move then
        if self.cursor_.y_position == "middle" then
            self.cursor_.y_position = "top";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y + 64));
        elseif self.cursor_.y_position == "top" then
            self.cursor_.y_position = "bottom";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y - 128));
        elseif self.cursor_.y_position == "bottom" then
            self.cursor_.y_position = "middle";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y + 64));
        end
    end

    if play_fx then
        audio.playSound("sounds/sfx_select.wav")
    end

end

function stage_select:move_down()
    local play_fx = true;
    local move = true;

    if self.cursor_.x_position == "left" or self.cursor_.x_position == "right" then
        play_fx = false
        move = false
    end

    if move then
        if self.cursor_.y_position == "middle" then
            self.cursor_.y_position = "bottom";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y - 64));
        elseif self.cursor_.y_position == "bottom" then
            self.cursor_.y_position = "top";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y + 128));
        elseif self.cursor_.y_position == "top" then
            self.cursor_.y_position = "middle";
            self.cursor_:set_position(cc.p(self.cursor_:get_position().x, self.cursor_:get_position().y - 64));
        end
    end

    if play_fx then
        audio.playSound("sounds/sfx_select.wav")
    end

end

function stage_select:onKeypad(event)

    local switch_sprite = false
    if not self.triggered_ then
        if event["pressed"]  then
            switch {
                ["KEY_KP_ENTER"] = function()

                    local selected_mug

                    for _, mug in pairs(self.mugs_) do
                        if mug:check_touch(self.cursor_:get_position()) then
                            selected_mug = mug
                        end
                    end

                    if selected_mug ~= nil then
                        local mug_index = selected_mug:get_image_index()
                        if mug_index == 4 then
                            audio.playSound("sounds/sfx_selected.wav")
                            self:getApp():enterScene("gameplay.levels." .. cc.levels[mug_index]["id"], "FADE", 1)
                            --self.triggered_ = true
                        else
                            audio.playSound("sounds/sfx_error.wav")
                        end

                    end




--
                end,
                ["KEY_LEFT_ARROW"] = function ()
                    switch_sprite = true
                    self:move_left()

                end,
                ["KEY_RIGHT_ARROW"] = function ()
                    switch_sprite = true
                    self:move_right()
                end,
                ["KEY_UP_ARROW"] = function ()
                    switch_sprite = true
                    self:move_up()

                end,
                ["KEY_DOWN_ARROW"] = function ()
                    switch_sprite = true
                    self:move_down()
                end,
                default = function ()  end,
            }:case(cc.KeyCodeKey[event["key"] + 1])
        end

    end

    if switch_sprite then
        self:set_cody_sprite()
    end

end


function stage_select:start()
    self:scheduleUpdate(handler(self, self.step))
    return self
end

function stage_select:stop()
    self:unscheduleUpdate()
    return self
end

function stage_select:step(dt)
    for _, scene_component in pairs(self.scene_components_) do
        scene_component:step(dt)
    end
    return self
end

function stage_select:onCleanup()
    self.keypad_layer_:removeKeypad():removeSelf()
    self.background_:destroy(true)
    self.text_:removeSelf()
    self:removeAllEventListeners()
end

return stage_select