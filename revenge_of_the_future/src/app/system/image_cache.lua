--
-- Created by Victor on 6/23/2015 9:34 AM
--

local image_cache = class("image_cache")


function image_cache:load_image(image_filename)
    return display.load_image(image_filename)
end

function image_cache:get_image(image_filename)
    return display.getImage(image_filename)
end

function image_cache:remove_image(image_filename)
    return display.removeImage(image_filename)
end



return image_cache

