--
-- Created by Victor on 6/24/2015 4:01 PM
--
return function(t)
t.case = function (self,x)
    local f=self[x] or self.default
    if f then
        if type(f)=="function" then
            f(x,self)
        else
            error("case "..tostring(x).." not a function")
        end
    end
end
return t
end
