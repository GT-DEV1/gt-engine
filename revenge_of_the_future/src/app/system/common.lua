--
-- Created by Victor on 6/23/2015 7:29 AM
--

local common = class("common")

common.events = {
    SCENE_END = "SCENE_END_EVENT",
}

return common;