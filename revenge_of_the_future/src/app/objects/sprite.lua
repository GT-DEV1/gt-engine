--
-- Created by Victor on 6/25/2015 8:34 PM
--

local spairs = import("app.system.spairs")


local sprite = class("sprite", function(data_filename, image_filename)

    local plist_dict = cc.FileUtils:getInstance():getValueMapFromFile(data_filename)
    local frames = {}
    for k, v in spairs(plist_dict["frames"]) do
        frames[#frames + 1] = k
    end

    display.loadSpriteFrames(data_filename, image_filename);

    local sprite = display.newSprite(display.newSpriteFrame(frames[1]))
    sprite.frames_ = frames
    sprite:getTexture():setAliasTexParameters()

    return sprite
end)


function sprite:ctor(data_filename, image_filename)
    self.touchRange_ = 0
    self:set_position(display.center)
end

function sprite:set_image_index(image_index)
    self.image_index_ = image_index
    self:setSpriteFrame(self.frames_[self.image_index_])
end

function sprite:get_image_index()
    return self.image_index_
end

function sprite:get_sprite_frame_name()
    return self.frames_[self.image_index_]
end

function sprite:set_position(point)
    self.position_ = point
    self:setPosition(point)
end

function sprite:get_position()
    return self.position_
end

function sprite:check_touch(point)
    local dx, dy = point.x - self.position_.x, point.y - self.position_.y
    local offset = math.sqrt(dx * dx + dy * dy)
    return offset <= self.touchRange_
end

--[[
function background:ctor(image_filename, x_frames, image_speed, position, rotation)

    self.position_ = position
    self.rotation_ = rotation
    self.image_speed_ = image_speed
    self.image_name_ = image_filename
    self.animation_ended_ = false;

    local texture = display.getImage(image_filename)
    local frameWidth = texture:getPixelsWide() / x_frames
    local frameHeight = texture:getPixelsHigh()

    -- create sprite frame based on image
    local frames = {}
    for i = 0, x_frames-1 do
        local frame = display.newSpriteFrame(texture, cc.rect(frameWidth * i, 0, frameWidth, frameHeight))
        frames[#frames + 1] = frame
    end

    -- create animation
    self.animation_ = display.newAnimation(frames, self.image_speed_)

    -- caching animation
    display.setAnimationCache(image_filename, self.animation_)

    if cc.debug_mode_ then
        self.drawNode = cc.DrawNode:create()
    end
end

if cc.debug_mode_ then

    function background:set_debug_draw_node(node)
        node:addChild(self.drawNode,1024)
    end

end

function background:destroy(remove_image)
    if remove_image then
        if display.getImage(self.image_name_) ~= nil then
            display.removeImage(self.image_name_)
        end

        if display.getAnimationCache(self.image_name_) ~= nil then
            display.removeAnimationCache(self.image_name_)
        end
    end

    self:removeSelf()
end

function background:animation_ended()
    return self.animation_ended_
end

function background:start(arguments)
    self:update_position()

    if arguments.is_loop then
        self:playAnimationForever(self.animation_)
    else
        if arguments.remove_self then
            self:playAnimationOnce(self.animation_, {onComplete = function(self) self.animation_ended_ = true end, removeSelf = true })
        else
            self:playAnimationOnce(self.animation_, {onComplete = function(self) self.animation_ended_ = true end })
        end
    end

    return self
end

function background:get_content_size()
    return self:getTexture():getContentSize()
end

function background:set_position(point)
    self.position_ = point
    self:update_position()
end

function background:get_position()
    return self.position_
end

function background:set_rotation(deg)
    self.rotation_ = deg
    self:update_position()
end

function background:get_rotation()
    return self.rotation_
end

local fixedDeltaTime = 1.0 / 60.0

function background:step(dt)

    if cc.debug_mode_ then
        local orig_point = cc.p(self:getBoundingBox().x, self:getBoundingBox().y)
        local dest_point = cc.p(orig_point.x + self:getBoundingBox().width, orig_point.y + self:getBoundingBox().height)

        self.drawNode:clear()
        self.drawNode:drawRect(orig_point, dest_point, cc.c4f(1.0,1.0,1.0,1.0))
    end

    return self
end

function background:check_touch(x, y)
    local dx, dy = x - self.position_.x, y - self.position_.y
    local offset = math.sqrt(dx * dx + dy * dy)
    return offset <= self.touchRange_
end

function background:update_position()
    self:move(self:get_position())
    :rotate(self:get_rotation())
end
]]--


return sprite
