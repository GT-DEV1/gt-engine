--
-- Created by Victor on 6/25/2015 3:44 PM
--

local text_element = class("text_element", function(text, font, size)

    local label = cc.Label:createWithTTF(text, font, 8)
                          :align(display.CENTER, display.center)
    label:getTexture()
         :setAliasTexParameters()

    return label
end)

function text_element:ctor(text, font, size)

--    self.position_ = position
--    self.image_speed_ = image_speed
--    self.image_name_ = image_filename
--    self.animation_ended_ = false;


--    local texture = display.getImage(image_filename)
--    local frameWidth = texture:getPixelsWide() / x_frames
--    local frameHeight = texture:getPixelsHigh()

    -- create sprite frame based on image
--    local frames = {}
--    for i = 0, x_frames-1 do
--        local frame = display.newSpriteFrame(texture, cc.rect(frameWidth * i, 0, frameWidth, frameHeight))
--        frames[#frames + 1] = frame
--    end

    -- create animation
--    self.animation_ = display.newAnimation(frames, self.image_speed_)

    -- caching animation
    --    display.setAnimationCache(filename, animation)
    if cc.debug_mode_ then
        self.drawNode = cc.DrawNode:create()
    end
end

if cc.debug_mode_ then

    function text_element:set_debug_draw_node(node)
        node:addChild(self.drawNode,1024)
    end

end

function text_element:destroy()
    self:removeSelf()
end

function text_element:animation_ended()
    return self.animation_ended_
end

function text_element:start(arguments)
    --self:update_position()
    --[[
    if arguments.is_loop then
        self:playAnimationForever(self.animation_)
    else
        if arguments.remove_self then
            self:playAnimationOnce(self.animation_, {onComplete = function(self) self.animation_ended_ = true end, removeSelf = true })
        else
            self:playAnimationOnce(self.animation_, {onComplete = function(self) self.animation_ended_ = true end })
        end
    end
    ]]--

    return self
end

function text_element:get_content_size()
    return self:getTexture():getContentSize()
end

function text_element:set_position(point)
    self.position_ = point
    self:update_position()
end

function text_element:get_position()
    return self.position_
end

local fixedDeltaTime = 1.0 / 60.0

function text_element:step(dt)

    --print(self:getSpriteFrame())
    --    self.dist_ = self.dist_ - self.speed_ * (dt / fixedDeltaTime)
    --    self.position_ = self:calcPosition(self.rotation_ + 180, self.dist_, self.destination_)

    if cc.debug_mode_ then
        local orig_point = cc.p(self:getBoundingBox().x, self:getBoundingBox().y)
        local dest_point = cc.p(orig_point.x + self:getBoundingBox().width, orig_point.y + self:getBoundingBox().height)

        self.drawNode:clear()
        self.drawNode:drawRect(orig_point, dest_point, cc.c4f(1.0,1.0,1.0,1.0))
    end

    return self
end

function text_element:update_position()
    self:move(self:get_position())
end

return text_element


