attribute vec3 in_Position;                  // (x,y,z)
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_texcoord;
varying vec4 v_color;

uniform sampler2D s_TexSampler;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_color = in_Colour;
    v_texcoord = in_TextureCoord;
}
//######################_==_YOYO_SHADER_MARKER_==_######################@~uniform vec2 uv_fire;
uniform vec2 uv_sine;
uniform vec4 uv_refract;

varying vec2 v_texcoord;
varying vec4 v_color;

uniform sampler2D s_TexSampler;

void main()
{


vec4 vertColour = v_color;
vec2 texcoord = v_texcoord;

vec4 combinedColour;

float offset = texcoord.y;
float mag = offset * uv_fire.x;

float sinescale = 1.0;

vec2 shift1, shift2;


shift1.x = sin((offset * uv_sine.x * sinescale) + uv_sine.y);

texcoord.x += shift1.x * mag;

//
vec4 refractTexColour = texture2D( s_TexSampler, texcoord );

combinedColour = vertColour * (refractTexColour * uv_refract);

gl_FragColor = combinedColour;

}

