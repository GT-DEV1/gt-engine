// game variables
global.game_status = "running";
global.animation_speeds = ds_map_create();
global.save_file = "a.dat";

global.lite_edition = true;

// player
player_setup();

// items
items_setup();

// levels
levels_setup();

// weapons
weapons_setup();

// animations
animations_setup();

// damage collision values
damage_collisions_setup();

// options
options_setup();

if(global.lite_edition){
    room_set_background(stage_select, 
                        0, 
                        1, 
                        0, 
                        bg_stageselect_lite, 
                        0, 
                        0, 
                        1, 
                        1, 
                        0, 
                        0, 
                        1.0);
}