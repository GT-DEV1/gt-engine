// returns a ds_list with the values from the query (if any)
// returns noone if no data was found
// argument0 must be an open sqlite database
// argument1 should be the query
// argument2 is data type: double or text

var qq = external_call(system_obj.f_query, argument0, argument1);
var list = ds_list_create();

while(external_call(system_obj.step, qq) = 1){ // fetch rows
    switch(argument2){
        case "double":
            ds_list_add(list, external_call(system_obj.d_double, qq, 0));
        break;
        
        case "text":
            ds_list_add(list, external_call(system_obj.d_text, qq, 0));
        break;
    }
}

external_call(system_obj.free,qq); // free the query

if(ds_list_empty(list)){
    ds_list_destroy(list);
    return noone;
} else {
    return list;
}