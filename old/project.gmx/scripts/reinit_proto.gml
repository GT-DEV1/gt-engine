with(proto){
    alive = true;
    shooting = false;
    charging = false;
    climbing = false;
    sliding = false;
    on_ground = true;
    in_door = false;
    stunned = false;
    
    vertical_speed = 0;
    horizontal_speed = 0;
    walk_speed = 2.5;
    climb_speed = 2.5;
    slide_speed = 5;
    max_fall_speed = 14;
    charge_power = "low";
    charge_fx_color = 0;
    
    stun_timer = 0;
    slide_timer = 0;
    shoot_timer = 0;
    charge_timer = 0;
    
    audio_stop_sound(sfx_charging1); 
    audio_stop_sound(sfx_charging2); 
}