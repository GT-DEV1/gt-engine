var buster_armor = ds_map_create();
var buster_helmet_armor = ds_map_create();
var xtreme_armor = ds_map_create();

// values below are animation speeds.

// buster
buster_armor[?buster_walk] = 0.3;
buster_armor[?buster_slide] = 0.3;
buster_armor[?buster_climb] = 0.3;
buster_armor[?buster_walkshoot] = 0.3;
buster_armor[?buster_hurt] = 0.95;

// buster - helmet
buster_helmet_armor[?buster_helmet_walk] = 0.3;
buster_helmet_armor[?buster_helmet_slide] = 0.3;
buster_helmet_armor[?buster_helmet_climb] = 0.3;
buster_helmet_armor[?buster_helmet_walkshoot] = 0.3;
buster_helmet_armor[?buster_helmet_hurt] = 0.95;

// xtreme
xtreme_armor[?xtreme_stand] = 0.3;
xtreme_armor[?xtreme_jump] = 0.5;
xtreme_armor[?xtreme_jumpshoot] = 0.2;
xtreme_armor[?xtreme_dashjump] = 0.5;
xtreme_armor[?xtreme_walk] = 0.3;
xtreme_armor[?xtreme_walkshoot] = 0.3;
xtreme_armor[?xtreme_hurt] = 0.95;

global.animations[?buster] = buster_armor;
global.animations[?buster_helmet] = buster_helmet_armor;
global.animations[?xtreme] = xtreme_armor;