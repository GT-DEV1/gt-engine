// pause animations
global.pause_sprites = ds_map_create();

global.pause_sprites[?buster] = spr_pause_buster_animation;
global.pause_sprites[?buster_helmet] = spr_pause_buster_helmet_animation;
global.pause_sprites[?fuzzy] = spr_pause_cat_animation;
global.pause_sprites[?sheriff] = spr_pause_star_animation;
global.pause_sprites[?vine] = spr_pause_vine_animation;
global.pause_sprites[?shield] = spr_pause_shield_animation;
global.pause_sprites[?xtreme] = spr_pause_xtreme_animation;