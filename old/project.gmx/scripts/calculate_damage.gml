// argument0 enemy
// argument1 bullet
var damage_map = global.damage_values[?argument0];
var weapon_map = damage_map[?argument1.object_index];
return weapon_map[?argument1.bullet_power];