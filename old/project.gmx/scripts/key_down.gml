if(!is_undefined(ds_map_find_value(global.key_map, argument0))){
    return keyboard_check(ds_map_find_value(global.key_map, argument0));
} else {
    show_message(argument0 + " does not exist in the key map.");
}