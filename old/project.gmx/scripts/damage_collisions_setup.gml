
//damage values

global.damage_values = ds_map_create();

var lyric_map = ds_map_create();
var cannonjoe_map = ds_map_create();
var flower_map = ds_map_create();
var sumatran_map = ds_map_create();
var subeil_map = ds_map_create();
var taban_map = ds_map_create();

// weapons
var buster_map = ds_map_create();
var xtreme_map = ds_map_create();

buster_map[?"low"]= 1;
buster_map[?"mid"]= 2;
buster_map[?"high"]= 3;

xtreme_map[?"low"]= 8;

lyric_map[?buster] = buster_map;
lyric_map[?xtreme] = xtreme_map;

cannonjoe_map[?buster] = buster_map;
cannonjoe_map[?xtreme] = xtreme_map;
flower_map[?buster] = buster_map;
flower_map[?xtreme] = xtreme_map;

sumatran_map[?buster] = buster_map;
sumatran_map[?xtreme] = xtreme_map;


subeil_map[?buster] = buster_map;
subeil_map[?xtreme] = xtreme_map;

taban_map[?buster] = buster_map;
taban_map[?xtreme] = xtreme_map;

global.damage_values[?lyric] = lyric_map;
global.damage_values[?cannonjoe] = cannonjoe_map;
global.damage_values[?flower] = flower_map;
global.damage_values[?sumatran] = sumatran_map;
global.damage_values[?subeil] = subeil_map;
global.damage_values[?taban] = taban_map;

// bosses
vineman_map = ds_map_create();
vineman_map[?buster] = buster_map;
vineman_map[?xtreme] = xtreme_map;

global.damage_values[?vineman] = vineman_map;