// argument0 should be a string with the file location. 

var savegame = ds_map_create();
// player
savegame[?"lives"] = global.player[?"lives"];

// item variables
savegame[?"e-tank"] = global.items[?"e-tank"];
savegame[?"m-tank"] = global.items[?"m-tank"];

// unlockables
savegame[?"c-helmet"]       = global.items[?"c-helmet"];
savegame[?"armor-arms"]     = global.items[?"armor-arms"];
savegame[?"armor-legs"]     = global.items[?"armor-legs"];
savegame[?"armor-chest"]    = global.items[?"armor-chest"];
savegame[?"armor-head"]     = global.items[?"armor-head"];

for(var i = 0; i<ds_list_size(global.levels); ++i){
    var global_level = global.levels[|i];
    var level_name = room_get_name(global_level[?"id"]);
    savegame[?level_name] = global_level[?"defeated"];
}

for(var i = 0; i<ds_list_size(global.weapons); ++i){
    var global_weapon = global.weapons[|i];
    var weapon_name = object_get_name(global_weapon[?"object"]);
    savegame[?weapon_name] = global_weapon[?"acquired"];
}

ds_map_secure_save(savegame, argument0);
ds_map_destroy(savegame);