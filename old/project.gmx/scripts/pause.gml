if(argument0){
    
    if(argument1){
        instance_create(0, 0, fadeout_pause);
    } else {
        global.game_status = "paused";
    }
    
} else {
        
    if(argument1){
        instance_create(0, 0, fadeout_unpause);
    } else {
        global.game_status = "running";
    }

}