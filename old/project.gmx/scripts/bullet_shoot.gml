//shoots a bullet, arguments are x offset, y offset, image_xscale from self and bullet type.
var offsets = ds_map_find_value(global.weapon_offsets, argument0);

var x_offset = x;
var y_offset = y;

if(image_xscale == 1){
    x_offset = x + ds_map_find_value(offsets, "x");
} else {
    x_offset = x - ds_map_find_value(offsets, "x");
}

y_offset = y + ds_map_find_value(offsets, "y");

var bullet = instance_create(x_offset, y_offset, argument0);
bullet.image_xscale = image_xscale;

return bullet;