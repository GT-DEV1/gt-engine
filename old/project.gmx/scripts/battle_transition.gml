with(proto){
    if(ds_map_find_value(global.current_weapon, "name") == "buster"){
        colors_swap('normal');
    }    
        
    //set gravity
    if (!climbing && vertical_speed < 16) {
        vertical_speed += ds_map_find_value(game_gravity, "current");
    }
    
    if (vertical_speed >= max_fall_speed) {
        vertical_speed = max_fall_speed;
    }
    
    
    //Horizontal Collision
    if (place_meeting(x+horizontal_speed,y,block_parent))
    {
        if(instance_place(x + horizontal_speed, y, block_parent).issolid == 1){
            while(!place_meeting(x+sign(horizontal_speed), y, block_parent)){
                x += sign(horizontal_speed);
            }
    //        x = round(x);
            horizontal_speed = 0;
        }
        
    }
    
    x += horizontal_speed;
    
    if(place_meeting(x, y-1, block_parent) && !on_ground && !sliding){//stuck
        if(instance_place(x, y-1, block_parent).issolid == 1){
            while(place_meeting(x, y-1, block_parent)){
                y += 1;
               
            }
        }
        
    }
    
    if (place_meeting(x,y+1,block_parent)){
        if(instance_place(x, y+1, block_parent).issolid == 1){
            if(!on_ground){
                on_ground = true;
                if(vertical_speed > 0){
                     var weapon_object = global.current_weapon[?"object"];
                     sprite_index = sprite_get_index(object_get_name(weapon_object) + "_" + "stand");
                }   
            }
        }
    } else {
        on_ground = false;
    }
    
    if (place_meeting(x, y+vertical_speed, block_parent) && vertical_speed > 0){
        if(instance_place(x, y+vertical_speed, block_parent).issolid == 1){
            
           
            while(!place_meeting(x, y+sign(vertical_speed), block_parent)){
                y += sign(vertical_speed);            
            }
            
            vertical_speed = 0;
        }
    }  
    
    
    y += vertical_speed;
    
    //Limit y value going above view
    if (vertical_speed < 0 && y <= view_yview-8) {y = view_yview-8;}
}