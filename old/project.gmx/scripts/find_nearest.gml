// returns nearest object in a given direction
// argument0 = x
// argument1 = y
// argument2 = object
// argument3 = direction
// argument4 = inheritance_filter
var pointx,pointy,object,dir, inheritance_filter, list,nearest;
pointx = argument0;
pointy = argument1;
object = argument2;
dir = argument3;
inheritance_filter = argument4;
list = ds_priority_create();
nearest = noone;

with (object) {
    var add_item = false;
    switch(dir){
        case "left":
            if(x < pointx){  
                add_item = true;
            }
        break;
        
        case "right":
            if(x > pointx){    
                add_item = true;
            }
        break;
        
        case "up":
            if(y < pointy){    
                add_item = true;
            }
        break;
        
        
        case "down":
            if(y > pointy){    
                add_item = true;
            }        
        break;
    }
    
    if(add_item){    
        if(inheritance_filter != noone){
            if(!object_is_ancestor(object_index, inheritance_filter)){
                if(visible){
                    ds_priority_add(list,id,distance_to_point(pointx,pointy));
                }
                
            }
        } else {
            if(visible){
                ds_priority_add(list,id,distance_to_point(pointx,pointy));
            }
            
            
        }
    
    }

    

}

if(!ds_priority_empty(list)){
    nearest = ds_priority_find_min(list);
}

ds_priority_destroy(list);

return nearest;