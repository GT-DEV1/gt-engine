// item variables
global.items = ds_map_create();

global.items[?"e-tank"] = 0;
global.items[?"m-tank"] = 0;

// unlockables
global.items[?"c-helmet"] = false;
global.items[?"armor-arms"] = false;
global.items[?"armor-legs"] = false;
global.items[?"armor-chest"] = false;
global.items[?"armor-head"] = false;