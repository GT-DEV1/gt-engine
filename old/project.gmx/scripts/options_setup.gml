// options
global.game_options = ds_map_create();
global.game_options[?"xtreme-activated"] = false;
global.game_options[?"c-helmet-activated"] = false;

// default setups
var buster_weapon = global.weapons[|0];

buster_weapon[? "acquired"] = true;   // default acquired weapon
buster_weapon[? "energy"] = -1;

global.current_weapon = buster_weapon;