/*
colors_swap('normal');
argument0 determines which color set to switch to.
*/

if (argument0 == 'charging1') { 
    global.frame = global.frame_1;
}

if (argument0 == 'charging2') {
    global.frame = global.frame_2;
}

if (argument0 == 'charging3') {
    global.frame = global.frame_3;
}

if (argument0 == 'charged1')
{
    global.frame = global.frame_c1;
    global.head = global.head_c1;
    global.body = global.body_c1;
}
if (argument0 == 'charged2')
{
    global.frame = global.frame_c2;
    global.head = global.head_c2;
    global.body = global.body_c2;
}

if (argument0 == 'normal')
{
    global.frame = global.frame_0;
    global.head = global.head_w0;
    global.body = global.body_w0;
}