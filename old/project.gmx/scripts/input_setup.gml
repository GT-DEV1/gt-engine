//Input/Options Variables
global.input = 0;
global.key_map = ds_map_create();
ds_map_add(global.key_map, "up", vk_up);
ds_map_add(global.key_map, "down", vk_down);
ds_map_add(global.key_map, "left", vk_left);
ds_map_add(global.key_map, "right", vk_right);
ds_map_add(global.key_map, "a", ord('Z'));
ds_map_add(global.key_map, "b", ord('X'));
ds_map_add(global.key_map, "start", vk_enter);