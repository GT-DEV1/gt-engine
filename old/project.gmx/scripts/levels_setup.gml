
// levels
global.levels = ds_list_create();
var level_ids;
var level_weapons;

level_ids[0] = level_freezerman;
level_ids[1] = level_sheriffman;
level_ids[2] = level_boomerman;
level_ids[3] = level_militaryman;
level_ids[4] = level_vineman;
level_ids[5] = level_shieldman;
level_ids[6] = level_nightman;
level_ids[7] = level_torchman;
level_ids[8] = level_test;

level_weapons[0] = freezer;
level_weapons[1] = sheriff;
level_weapons[2] = boomer;
level_weapons[3] = military;
level_weapons[4] = vine;
level_weapons[5] = shield;
level_weapons[6] = night;
level_weapons[7] = torch;
level_weapons[8] = noone;

for(var i = 0; i<array_length_1d(level_ids); ++i){
    level_map = ds_map_create();
    level_map[? "id"] = level_ids[i];
    level_map[? "defeated"] = false;
    level_map[? "weapon"] = level_weapons[i];

    ds_list_add(global.levels, level_map);
    ds_list_mark_as_map(global.levels, i);
}

global.current_level = noone;
// levels
global.levels = ds_list_create();
var level_ids;
var level_weapons;

level_ids[0] = level_freezerman;
level_ids[1] = level_sheriffman;
level_ids[2] = level_boomerman;
level_ids[3] = level_militaryman;
level_ids[4] = level_vineman;
level_ids[5] = level_shieldman;
level_ids[6] = level_nightman;
level_ids[7] = level_torchman;
level_ids[8] = level_test;

level_weapons[0] = freezer;
level_weapons[1] = sheriff;
level_weapons[2] = boomer;
level_weapons[3] = military;
level_weapons[4] = vine;
level_weapons[5] = shield;
level_weapons[6] = night;
level_weapons[7] = torch;
level_weapons[8] = noone;

for(var i = 0; i<array_length_1d(level_ids); ++i){
    level_map = ds_map_create();
    level_map[? "id"] = level_ids[i];
    level_map[? "defeated"] = false;
    level_map[? "weapon"] = level_weapons[i];

    ds_list_add(global.levels, level_map);
    ds_list_mark_as_map(global.levels, i);
}

global.current_level = noone;