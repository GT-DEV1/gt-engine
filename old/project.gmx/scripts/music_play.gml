/*
music_play(bgm_index,'loop');
argument0 = the index from the music
argument1 = way to play the music: 'loop' or 'play'
*/

with (system_obj)
{
    global.bgmusic = argument0;
    if (argument1 == "loop") audio_play_sound(argument0, 10, true);
    if (argument1 == "play") audio_play_sound(argument0, 10, false);

}