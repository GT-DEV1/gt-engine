var vineman_map = ds_map_create();

// values below are animation speeds.
vineman_map[?vineman_stand] = 0.0;
vineman_map[?vineman_jump] =  0.2;
vineman_map[?vineman_standshoot_a] =  0.3;
vineman_map[?vineman_standshoot_b] =  0.2;

global.animations[?vineman] = vineman_map;