/*
music_init();
Called at game start; initiates all music variables
This way if you change a music track in some way it
only needs updating here and it will pass to the rest
of the game.
*/
//=========Menu's_Music===========
global.music_title = mus_title;
global.music_password = mus_password;
global.music_stageselect = mus_stageselect;
//=========Enemies_Music===================
global.music_bossintro = mus_bossintro;
global.music_napalm = mus_napalm;
global.music_sheriffman = mus_sheriffman;
global.music_boss1 = mus_boss1;
//==========Misc_Music=================
global.music_victory = mus_victory;
global.music_getweapon = mus_getweapon;
global.music_gameover = mus_gameover;