if(file_exists(argument0)){
    var savegame = ds_map_secure_load(argument0);
    // player 
    global.player[?"lives"] = savegame[?"lives"];
    
    // item variables
    global.items[?"e-tank"] = savegame[?"e-tank"];
    global.items[?"m-tank"] = savegame[?"m-tank"];
    
    // unlockables
    global.items[?"c-helmet"]       = savegame[?"c-helmet"];
    global.items[?"armor-arms"]     = savegame[?"armor-arms"];
    global.items[?"armor-legs"]     = savegame[?"armor-legs"];
    global.items[?"armor-chest"]    = savegame[?"armor-chest"];
    global.items[?"armor-head"]     = savegame[?"armor-head"];
       
    for(var i = 0; i<ds_list_size(global.levels); ++i){
        var global_level = global.levels[|i];
        var level_name = room_get_name(global_level[?"id"]);
        global_level[? "defeated"] = savegame[?level_name];
    }
    
    for(var i = 0; i<ds_list_size(global.weapons); ++i){
        var global_weapon = global.weapons[|i];
        var weapon_name = object_get_name(global_weapon[?"object"]);
        global_weapon[?"acquired"] = savegame[?weapon_name];
    }
    
    ds_map_destroy(savegame);
} else {
    save_game(argument0);
}