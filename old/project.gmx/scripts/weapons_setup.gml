// weapons
global.weapons = ds_list_create();

// weapon instance objects
var weapon_objects;
weapon_objects[0] = buster;
weapon_objects[1] = fuzzy;
weapon_objects[2] = freezer;
weapon_objects[3] = sheriff;
weapon_objects[4] = boomer;
weapon_objects[5] = military;
weapon_objects[6] = vine;
weapon_objects[7] = shield;
weapon_objects[8] = night;
weapon_objects[9] = torch;
weapon_objects[10] = xtreme;


// translation strings
weapon_translations[0] = "STR_VIOLET_BROWNER";
weapon_translations[1] = "STR_FUZZY_BROWNER";
weapon_translations[2] = "STR_FREEZER_BROWNER";
weapon_translations[3] = "STR_SHERIFF_BROWNER";
weapon_translations[4] = "STR_BOOMER_BROWNER";
weapon_translations[5] = "STR_MILITARY_BROWNER";
weapon_translations[6] = "STR_VINE_BROWNER";
weapon_translations[7] = "STR_SHIELD_BROWNER";
weapon_translations[8] = "STR_NIGHT_BROWNER";
weapon_translations[9] = "STR_TORCH_BROWNER";
weapon_translations[10] = "STR_XTREME_BROWNER";


for(var i = 0; i<array_length_1d(weapon_objects); ++i){
    weapon_map = ds_map_create();
    weapon_map[?"object"] = weapon_objects[i];
    weapon_map[?"acquired"] = false;
    weapon_map[?"energy"] = 28;
    weapon_map[?"translation"] = weapon_translations[i];

    ds_list_add(global.weapons, weapon_map);
    ds_list_mark_as_map(global.weapons, i);
}

// weapon offsets
global.weapon_offsets = ds_map_create();

// player
buster_offsets = ds_map_create();
xtreme_offsets = ds_map_create();
cannon_joe_offsets = ds_map_create();
vineman_offsets_a = ds_map_create();
vineman_offsets_b = ds_map_create();

buster_offsets[?"x"] = 16;
buster_offsets[?"y"] = -10;

xtreme_offsets[?"x"] = 16;
xtreme_offsets[?"y"] = -30;

// enemy

cannon_joe_offsets[?"x"] = -21;
cannon_joe_offsets[?"y"] = -18;

// boss
vineman_offsets_a[?"x"] = 0;
vineman_offsets_a[?"y"] = -16;

vineman_offsets_b[?"x"] = 0;
vineman_offsets_b[?"y"] = -12;


global.weapon_offsets[?buster] = buster_offsets;
global.weapon_offsets[?xtreme] = xtreme_offsets;
global.weapon_offsets[?cannonjoe_bullet] = cannon_joe_offsets;
global.weapon_offsets[?vineman_bullet_a] = vineman_offsets_a;
global.weapon_offsets[?vineman_bullet_b] = vineman_offsets_b;